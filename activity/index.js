// console.log("Eyy World")

// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:
const getCube = 4 ** 3
console.log(`The cube of 4 is ${getCube}`)



	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:
const address = ["69 ", " Baryo Walang Tao", " Brgy. Tahimik", " Otso City"]

const [houseNum, street, brgy, city] = address
console.log(`I live at ${address}.`)

	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:
const animal = {
	animalName: "Junior",
	animalKind: "Ahas Tulog",
	animalSize: "9 in"
}

const {animalName, animalKind, animalSize} = animal
function getAnimalDetails(name, kind, size) {
	console.log(`${name} is my favorite pet. He is an ${kind} and his size is about ${size}.`)
}
getAnimalDetails(animalName, animalKind, animalSize)
	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:
const numbers = [2, 4, 6, 8]

numbers.forEach((number) => console.log(number));

/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:
class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}
const myDog = new Dog()

myDog.name = "Taybants",
myDog.age = 2,
myDog.breed = "Aspin"

console.log(myDog)